<!doctype html>
<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

    <nav class="topnav">
        <a href="/" class="topnav__link">Список пользователей к продуктам</a>
        <a href="/users/add" class="topnav__link">Добавить пользователя</a>
        <a href="/users/ptu" class="topnav__link">Добавить связь пользователя к продукту</a>
    </nav>

<div class="container">
    <div class="row">
    @include('flash-message')
    </div>
    <div class="row">
        <div class="col-lg-12"> @yield('content') </div>
    </div>
</div>

</body>
</html>
