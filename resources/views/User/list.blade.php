@extends('layouts.master')

@section('content')
    Список пользователей
<ul>
@forelse($users as $user)
    <li><a href="/users/{{$user->id }}">{{ $user->firstName }}</a>
    @forelse($user->products as $product)
        - {{ $product->name }}
        @empty
            - нет продуктов
    @endforelse
    </li>
@empty
        <li>Нет пользователей</li>
@endforelse
</ul>
@endsection
