@extends('layouts.master')

@section('content')
    <div class="well">

        {!! Form::open(['url' => '/users/ptu', 'method' => 'POST', 'class' => 'form-horizontal']) !!}

        <fieldset>

            <legend>Связь пользователя с продуктами</legend>

            <!-- Select With One Default -->
            <div class="form-group">
                {!! Form::label('User', 'Select w/Default', ['class' => 'col-lg-2 control-label'] )  !!}
                <div class="col-lg-10">
                    {!!  Form::select('User', $users ,  '', ['class' => 'form-control' ]) !!}
                </div>
            </div>

            <!-- Select Multiple -->
            <div class="form-group">
                {!! Form::label('multipleselect[]', 'Multi Select', ['class' => 'col-lg-2 control-label'] )  !!}
                <div class="col-lg-10">
                    {!!  Form::select('Product[]', $products, $selected_ = null, ['class' => 'form-control', 'multiple' => 'multiple']) !!}
                </div>
            </div>

            <!-- Submit Button -->
            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
                </div>
            </div>

        </fieldset>

        {!! Form::close()  !!}

    </div>
@endsection


