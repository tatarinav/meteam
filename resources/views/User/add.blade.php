@extends('layouts.master')

@section('content')
    <div class="well">

        {!! Form::open(['url' => '/users/add', 'method' => 'POST', 'class' => 'form-horizontal']) !!}

        <fieldset>

            <legend>Добавить пользователя</legend>

            <!-- Email -->
            <div class="form-group">
                {!! Form::label('firstName', 'Имя:', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('firstName', $firstName = null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('lastName', 'Фамилия:', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('lastName', $lastName = null, ['class' => 'form-control', 'placeholder' => 'Фамилия']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('age', 'Возраст:', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::number('age', $age = null, ['class' => 'form-control', 'placeholder' => 'Возраст']) !!}
                </div>
            </div>


            <!-- Submit Button -->
            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
                </div>
            </div>

        </fieldset>

        {!! Form::close()  !!}

    </div>
@endsection
