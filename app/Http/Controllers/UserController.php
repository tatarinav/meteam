<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //
    public function index()
    {
        return view('user.list', ['users' => User::all()]);
    }

    public function create()
    {
        return view('user.add');
    }

    public function store (Request $request)
    {
        $validator = Validator::make($request->all(),[
            'firstName' => 'required|string|max:128',
            'lastName' => 'required|string|max:128',
            'age' => 'required|numeric|max:128',
        ]);

        if ($validator->fails()) {
            return back()->with('error',$validator->errors());
        }

        User::create($request->all());

        return back()->with('success','Пользователь добавлен');
    }

    public function productToUser()
    {
        return view('user.product_to_user', ['users' => User::all()->pluck('firstName', 'id'), 'products' => Product::all()->pluck('name','id')]);
    }

    public function productToUserUpdate(Request $request)
    {
        $data = $request->all();
        $user = User::findOrFail($data['User']);

        if (!$user) {
            return back()->with('error','пользователь не найден');
        }
        $products = Product::findOrFail($data['Product']);

        if (!$products) {
            return back()->with('error','продукты не найдены');
        }

        $user->products()->sync($products);
        return back()->with('success','Связь добавлена');
    }

}
